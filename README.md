# Battery Level Kernel Module

This is a simple kernel module that reads information about battery
current and full levels. Module does it in separate thread.

It dispatches information via netlink protocol.

Tested on Linux Kernel version 4.4.0-146.
