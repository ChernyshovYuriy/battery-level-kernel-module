#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/power_supply.h>
#include <linux/delay.h>
#include <linux/kthread.h>
#include <net/sock.h>

/**
 * Poll interval: 5 seconds.
 */
#define POLL_INTERVAL_DEFAULT (5 * 1 * HZ)
/**
 * The netlink protocol number.
 */
#define NETLINK_TEST           31
/**
 * Size of a buffer needed to pass data back to client.
 */
#define BUFSIZ                 1000

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Yurii Chernyshov");
MODULE_DESCRIPTION("Linux module to track battery level.");
MODULE_VERSION("0.01");

static const int OK = 0;
static const char name[4] = "BAT0";
static const char thread_name[25] = "lkm_battery_level_thread";
static const char delimiter[1] = "/";
static union power_supply_propval chargenow, chargefull;
static struct task_struct *battery_thread = NULL;
static struct power_supply *psy = NULL;
static const int poll_interval = POLL_INTERVAL_DEFAULT;
static struct sock *nl_sk = NULL;

static void nl_recv_msg(struct sk_buff *skb) {
    struct nlmsghdr *nlh;
    int pid;
    struct sk_buff *skb_out;
    int msg_size;
    char msg[BUFSIZ];
    snprintf(msg, sizeof(msg), "%d%s%d", chargenow.intval, delimiter, chargefull.intval);
    int res;

    msg_size = strlen(msg);

    nlh = (struct nlmsghdr*)skb->data;
    printk(KERN_INFO "Battery module - netlink received msg:%s\n", (char*)nlmsg_data(nlh));
    pid = nlh->nlmsg_pid; /*pid of sending process */

    skb_out = nlmsg_new(msg_size, 0);

    if (!skb_out) {
        printk(KERN_ERR "Battery module - failed to allocate new skb\n");
        return;
    }
    nlh = nlmsg_put(skb_out, 0, 0, NLMSG_DONE, msg_size, 0);
    NETLINK_CB(skb_out).dst_group = 0; /* not in mcast group */
    strncpy(nlmsg_data(nlh), msg, msg_size);

    res = nlmsg_unicast(nl_sk,skb_out,  pid);

    if (res < 0) {
        printk(KERN_ERR "Battery module - error while sending back to user\n");
    }
}

/**
 * Thread function to handle battery level in.
 */
static int thread_fn(void *arguments) {
    printk(KERN_INFO "Battery module - thread starting\n");
    allow_signal(SIGKILL);
    for (;;) {
        set_current_state(TASK_INTERRUPTIBLE);
        schedule_timeout(poll_interval);
        if (kthread_should_stop()) {
            break;
        }
        if (power_supply_get_property(psy, POWER_SUPPLY_PROP_CHARGE_NOW, &chargenow)) {
            continue;
        }
        if (power_supply_get_property(psy, POWER_SUPPLY_PROP_CHARGE_FULL, &chargefull)) {
            continue;
        }
        printk(KERN_INFO "Battery module - (%d / %d)\n", chargenow.intval, chargefull.intval);
    }
    printk(KERN_INFO "Battery module - thread stopping\n");
    do_exit(OK);
    return OK;
}

/**
 * Entry point to module.
 */
static int __init lkm_example_init(void) {
    printk(KERN_INFO "Battery module - init\n");
    psy = power_supply_get_by_name(name);
    battery_thread = kthread_create(thread_fn, NULL, thread_name);

    // For kernels 3.6 and above.
    struct netlink_kernel_cfg cfg = {
            .input = nl_recv_msg
    };
    nl_sk = netlink_kernel_create(&init_net, NETLINK_TEST, &cfg);
    if (IS_ERR(battery_thread)) {
        printk(KERN_ERR "Battery module - couldn't create thread\n");
    } else {
        wake_up_process(battery_thread);
    }
    printk(KERN_INFO "Battery module - init completed\n");
    return OK;
}

/**
 * Exit point from module
 */
static void __exit lkm_example_exit(void) {
    printk(KERN_INFO "Battery module - exit\n");
    if (nl_sk) {
        netlink_kernel_release(nl_sk);
        printk(KERN_INFO "Battery module - netlink released\n");
    }
    if (battery_thread) {
        kthread_stop(battery_thread);
        printk(KERN_INFO "Battery module - thread stopped");
    }
}

module_init(lkm_example_init);
module_exit(lkm_example_exit);
